require('dotenv').config();
const urljoin = require('url-join');
const rl = require('readline-sync');

const auth = require('solid-auth-cli');
const FileClient = require('solid-file-client');
const fc = new FileClient(auth);
const generateAclFiles = require('./generate-acl');

// auth
const idp = process.env.SOLID_IDP;
const username = process.env.SOLID_USERNAME;

// sources
const workingDir = 'file://' + process.cwd();
const buildDir = urljoin(workingDir, 'build/');
const staticDir = urljoin(buildDir, 'static/');

// targets
const deployUrl = urljoin(process.env.PUBLIC_URL, '/'); // ensure trailing slash
const staticUrl = urljoin(deployUrl, 'static/');

async function run() {
    try {
        const password = rl.question(
            `Password for user "${username}" at ${idp}: `,
            {
                hideEchoBack: true,
            }
        );

        console.log('logging in...');
        const session = await auth.login({ idp, username, password });

        generateAclFiles(session.webId);

        console.log('overwrite static dir contents from', staticDir, 'to', staticUrl, '...');
        await fc.copyFolder(staticDir, staticUrl);

        console.log('uploading sources from', buildDir, 'to', deployUrl, '...');
        await fc.copyFolder(buildDir, deployUrl, { merge: 'keep_source' });

        console.log('finished sucessfully!');
    } catch (err) {
        console.log(err);
        console.error('Error :( Details above.');
    }
}

if (!deployUrl) {
    console.error(
        'You have to set the PUBLIC_URL env variable to the location you want to deploy to',
    );
} else {
    run();
}
