const Handlebars = require('handlebars');
const fs = require('fs');
const path = require('path');

const aclTemplates = {
    staticFolder: `
# ACL for the static folder
# Public-readable

@prefix acl:  <http://www.w3.org/ns/auth/acl#> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .

<#owner>
    a acl:Authorization ;
    acl:agent <{{owner}}> ;
    acl:accessTo <./> ;
    acl:default <./> ;
    acl:mode acl:Read, acl:Write, acl:Control .

<#public>
    a acl:Authorization ;
    acl:agentClass foaf:Agent ;
    acl:accessTo <./> ;
    acl:default <./> ;
    acl:mode acl:Read .
`,
    manifestJson: `
# ACL for the default manifest.json resource
# Public-readable

@prefix acl:  <http://www.w3.org/ns/auth/acl#> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .

<#owner>
    a acl:Authorization ;
    acl:agent <{{owner}}> ;
    acl:accessTo <manifest.json> ;
    acl:mode acl:Read, acl:Write, acl:Control .

<#public>
    a acl:Authorization ;
    acl:agentClass foaf:Agent ;
    acl:accessTo <manifest.json> ;
    acl:mode acl:Read .
`,
};

const staticFolderAclFilename = path.join(
    process.cwd(),
    'build',
    'static',
    '.acl'
);

const manifestJsonAclFilename = path.join(
    process.cwd(),
    'build',
    'manifest.json.acl'
);

module.exports = webId => {
    var data = { owner: webId };
    console.log(
        'generating ACL files with owner',
        webId,
        'granting public read access'
    );
    var staticFolderAcl = Handlebars.compile(aclTemplates.staticFolder)(data);
    var manifestJsonAcl = Handlebars.compile(aclTemplates.manifestJson)(data);

    console.log('writing file', staticFolderAclFilename);
    fs.writeFileSync(staticFolderAclFilename, staticFolderAcl);
    console.log('writing file', manifestJsonAclFilename);
    fs.writeFileSync(manifestJsonAclFilename, manifestJsonAcl);
};
