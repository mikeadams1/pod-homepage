import { Badge, Card, List } from 'antd';
import React from 'react';

const data = [
    {
        title: 'Profile',
        description: 'Solid WebID Profile',
        href: '/profile/card',
        status: 'public',
    },
    {
        title: 'Slides',
        description: 'Slides from my talks',
        href: '/slides',
        status: 'public',
    },
    {
        title: 'Inbox',
        description: 'Solid Inbox',
        href: '/inbox',
        status: 'private',
    },
    {
        title: 'Public Folder',
        description: 'Public data available in this pod',
        href: '/public',
        status: 'public',
    },
    {
        title: 'Private Folder',
        description: 'Private data with restricted access',
        href: '/private',
        status: 'private',
    },
];

export default () => (
    <Card title="Solid Data">
        <List
            dataSource={data}
            renderItem={item => (
                <List.Item>
                    <List.Item.Meta
                        title={<a href={item.href}>{item.title}</a>}
                        description={item.description}
                    />
                    <div>
                        <Badge
                            status={
                                item.status === 'public' ? 'success' : 'error'
                            }
                            text={item.status}
                        />
                    </div>
                </List.Item>
            )}
        />
    </Card>
);
