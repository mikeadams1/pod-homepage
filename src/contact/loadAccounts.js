import data from '@solid/query-ldflex';

export const loadAccounts = async (webId) => {
    const result = [];
    for await (const uri of data[webId].foaf_account) {
        const name = await data[uri].sioc_name;
        const homepage = await data[uri].foaf_homepage;
        const serviceName = await data[uri].foaf_accountServiceHomepage.label;
        result.push({
            name: name ? `${name}` : `${uri}`,
            homepage: homepage && `${homepage}`,
            serviceName: serviceName &&`${serviceName}`
        });
    }
    return result;
}