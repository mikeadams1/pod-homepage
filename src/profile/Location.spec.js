import { shallow } from 'enzyme';
import { Location } from './Location';
import React from 'react';
import { Icon } from 'antd';

describe('Location', () => {
    it('should show locality and country name', () => {
        const result = shallow(<Location locality="London" country="United Kingdom"/>);
        expect(result).toContainReact(<Icon type="environment"/>);
        expect(result).toIncludeText('London, United Kingdom');
    });

    it('should show only locality', () => {
        const result = shallow(<Location locality="London"/>);
        expect(result).toContainReact(<Icon type="environment"/>);
        expect(result).toHaveText('<Icon /> London');
    });

    it('should show only country', () => {
        const result = shallow(<Location country="United Kingdom"/>);
        expect(result).toContainReact(<Icon type="environment"/>);
        expect(result).toHaveText('<Icon /> United Kingdom');
    });

    it('should render nothing if all information is missing', () => {
        const result = shallow(<Location />);
        expect(result).toBeEmptyRender();
    });
});