import data from '@solid/query-ldflex';

export const loadAddress = async webId => {
    const hasAddress = await data[webId].vcard_hasAddress;
    if (!hasAddress) return {};

    const address = data[`${hasAddress}`];
    const locality = await address.vcard_locality;
    const country = await address['vcard:country-name'];

    return {
        locality: locality && `${locality}`,
        country: country && `${country}`,
    };
};
