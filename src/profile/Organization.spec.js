import { Organization } from './Organization';
import { shallow } from 'enzyme';
import { Icon } from 'antd';
import React from 'react';

describe('Organization', () => {
    describe('if present', () => {
        it('should render icon', () => {
            const result = shallow(<Organization name="Test" />);
            expect(result).toContainReact(<Icon type="laptop" />);
        });

        it('should render name', () => {
            const result = shallow(<Organization name="Test" />);
            expect(result).toIncludeText('Test');
        });
    });

    it('does not render if missing', () => {
        const result = shallow(<Organization />);
        expect(result).toBeEmptyRender();
    });
});
