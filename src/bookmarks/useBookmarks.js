import { loadBookmarks } from './loadBookmarks';
import { usePublicTypeIndex } from './usePublicTypeIndex';
import { useAsync } from 'react-use';

export const useBookmarks = webId => {
    const doc = usePublicTypeIndex(
        webId,
        'http://www.w3.org/2002/01/bookmark#Bookmark'
    );

    const { value, loading, error } = useAsync(() => loadBookmarks(doc), [doc]);

    return {
        bookmarks: value || [],
        loading: doc ? loading : true,
        error,
    };
};
