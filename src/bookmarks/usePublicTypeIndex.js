import data from '@solid/query-ldflex';
import { loadTypeRegistration } from './loadTypeRegistration';
import { useAsync } from 'react-use';

export const usePublicTypeIndex = (webId, classIri) => {
    const { value } = useAsync(async () => {
        const index = await data[webId].solid_publicTypeIndex;
        return loadTypeRegistration(index, classIri);
    }, [webId, classIri]);

    return value;
};
