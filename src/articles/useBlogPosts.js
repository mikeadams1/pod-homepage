import loadPostsFromAllBlogs from './loadPostsFromAllBlogs';
import { useAsync } from 'react-use';

export const useBlogPosts = webId => {
    const { value, loading, error } = useAsync(
        () => loadPostsFromAllBlogs(webId),
        [webId]
    );

    return {
        posts: value || [],
        loading,
        error,
    };
};
